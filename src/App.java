public class App {
    public static void main(String[] args) throws Exception {
        
        //Crear/instanciar un objeto
        Persona objPersona_1 = new Persona("Andres", "Quintero");
        //Acceder al nombre y apellido
        String nombre = objPersona_1.getNombre();
        String apellido = objPersona_1.getApellido();
        System.out.println("Nombre: "+nombre);
        System.out.println("Apellido: "+apellido);

        Persona objPersona_2 = new Persona("Juliana", "Torres");
        System.out.println( objPersona_2.getNombre()+" "+objPersona_2.getApellido() );
        objPersona_2.setNombre("Laila");
        objPersona_2.setApellido("Hernandez");
        System.out.println( objPersona_2.getNombre()+" "+objPersona_2.getApellido() );

        //Asignar edad a persona 1 y 2
        objPersona_1.setEdad(28);
        objPersona_2.setEdad(29);

        //Calcular numero suerte
        int n_suerte1 = objPersona_1.calcular_numero_suerte(20, 12);
        int n_suerte2 = objPersona_2.calcular_numero_suerte(15, 07);

        System.out.println("Numero de la suerte de "+objPersona_1.getNombre()+": "+n_suerte1);
        System.out.println("Numero de la suerte de "+objPersona_2.getNombre()+": "+n_suerte2);
    }
}
