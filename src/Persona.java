public class Persona {
    //ATRIBUTOS
    private String nombre;
    private String apellido;
    private int edad;
    
    //CONSTRUCTOR
    public Persona(String nombre, String apellido){ 
        //this.nombre -> Atributo
        //nombre -> Parámetro
        this.nombre = nombre;
        this.apellido = apellido;
        //System.out.println("Hola soy una persona, soy "+nombre+" "+apellido);
    }

    //MÉTODOS CONSULTORES
    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }

    public int getEdad(){
        return edad;
    }

    //MÉTODOS MODIFICADORES
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public void setEdad(int edad){
        this.edad = edad;
    }

    //ACCIONES
    public int calcular_numero_suerte(int dia, int mes){
        int numero_suerte = (edad * mes) / dia;
        return numero_suerte;
    }

}
